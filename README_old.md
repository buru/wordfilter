#wordfilter

配置文件
-----------------------------------------------------------------------------------
- 默认位置：`config/wordfilter.properties`
- 服务端口：默认`9119`，可通修改property参数`sardine.port=9110` 指定其他端口
- 应用授权：默认全部，修改property参数`security.apps`指定，多个app之间逗号分隔
- 注意：修改该文件需要重启服务器

敏感词字典
-----------------------------------------------------------------------------------
- 默认位置：`config/dics/*.dic`，该目录下所有`*.dic`文件都会被加载，默认utf-8编码，每一行是一个敏感词
- 修改配置：当对该目录下的文件作任意修改时，包括增删改任意文件或目录，都会自动重新加载。
- 过滤级别：`config/wordfilter.properties/wordfilter.level.*=dic1.dic,dic2.dic`指定级别，以及级别所对应的字典，多个字典逗号分隔。

日志格式
-----------------------------------------------------------------------------------
- 日志配置文件：`config/logback.xml`
- 日志文件目录：`logs/*.log`


服务接口
-----------------------------------------------------------------------------------

###1、内容过滤

#### API

**Request**  `application/json`
```
POST /security?appId=sardine&subject=word_filter

{
    action:"动作类型",  # verify | replace
    level:"过滤级别",   # sms，通过properties文件中的过滤级别设置
    data:"待过滤内容"
}
```

**Response**  `application/json`
```
{
    "success": true,
    "result": {
        具体的返回值包装，请求类型不一样返回值包装不一样
    }
}
```

#### 示例

**Request**
```
POST http://172.20.10.222:9110/security?appId=sardine&subject=word_filter

{
    action:"replace",
    level:"sms",
    data:"PcP气枪ll/网；；报。。13423205670。。的世界  职65645业报;;;;仇四大是单身"
}
```

**Response**
```
{
    "success": true,
    "result": {
        "legal": false,
        "legalContent": "PcP气*ll/网；；报。。***********。。的世界  职65645业报;;;;仇四大是单身"
    }
}
```

压力测试
-----------------------------------------------------------------------------------

- 硬件：双核 3G win7_32

- 字典大小：846
- 测试文本大小：3k

- 并发：30
- 总请求：10000

> ab -k -n 10000 -c 30 -p wf  http://localhost:9110/filter/sardine/replace

```
Concurrency Level:      30
Time taken for tests:   11.349 seconds
Complete requests:      10000
Failed requests:        0
Write errors:           0
Keep-Alive requests:    10000
Total transferred:      1091635 bytes
Total POSTed:           34906375
HTML transferred:       140210 bytes
Requests per second:    881.17 [#/sec] (mean)
Time per request:       34.046 [ms] (mean)
Time per request:       1.135 [ms] (mean, across all concurrent requests)
Transfer rate:          93.94 [Kbytes/sec] received
                        3003.76 kb/s sent
                        3097.70 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       1
Processing:     4   34  30.4     28     564
Waiting:        2   33  30.4     27     562
Total:          4   34  30.5     28     564

Percentage of the requests served within a certain time (ms)
  50%     28
  66%     32
  75%     36
  80%     40
  90%     53
  95%     65
  98%    118
  99%    140
 100%    564 (longest request)
```