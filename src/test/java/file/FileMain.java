package file;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @auth bruce-sha
 * @date 2015/6/24
 */
public class FileMain {
    public static void main(String[] args)throws  IOException{
        FileSystem f = FileSystems.getDefault();
        Path path = f.getPath("README.md");
        System.out.println(Files.readAllLines(path));
    }

}
