package file;

import java.util.stream.IntStream;

/**
 * @auth bruce-sha
 * @date 2015/6/25
 */
public class IntStreamTest {
    public static void main(String[] args) {

        IntStream.range(0, 10)
                .parallel()
                .forEach(i -> System.out.print(i));

    }
}
