package file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @auth bruce-sha
 * @date 2015/6/23
 */
public class FilesTest {
    public static final String DIC_PATH = "";

    public static void readFiles() throws IOException {

        System.out.println(FileSystems.getDefault());

        Path path = Paths.get("/test.txt");

//        path =FileSystems.getDefault().getPath("logs", "access.log");
//          path = Paths.get("D:\\jd.txt");

        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

        // Java8用流的方式读文件，更加高效
        Files.lines(path, StandardCharsets.UTF_8).forEach(System.out::println);
    }

    public static void main(String[] args)  throws IOException{
        readFiles();
    }

}
