package json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @auth bruce-sha
 * @date 2015/7/7
 */
public class JsonTest {
    public static void main(String[] args) {
        String body = "{action:'replace',level:'simple'}";
        JSONObject o = JSON.parseObject(body);

        System.out.println(o);
    }
}
