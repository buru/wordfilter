package back;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @auth bruce-sha
 * @date 2015/7/8
 */
public class TimeTest {
    public static void main(String[] args) {
        System.out.println( LocalDate.now());
        System.out.println( LocalTime.now());
        System.out.println( LocalDateTime.now());
    }
}
