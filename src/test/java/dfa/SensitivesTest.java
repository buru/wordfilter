package dfa;

import wordfilter.dfa.Sensitives;

import java.io.IOException;

/**
 * @auth bruce-sha
 * @date 2015/6/24
 */
public class SensitivesTest {

    public static void main(String[] args) throws IOException {
        final Sensitives s = Sensitives.singleton("");
        long st=System.currentTimeMillis();

//        s.build();

        System.out.println(System.currentTimeMillis()-st);
        String content = "据说，哦！你有点逗比，约  炮，约////炮。江，泽民和江青黑没有任何关系，薄熙来也是个大美女。";
        content+=content;
        content+=content;
        content+=content;
        content+=content;
        System.out.println(content);
//        String content = "，江泽民";
//        content="1";
        st=System.currentTimeMillis();
        System.out.println(s.judge("",content));
        System.out.println(System.currentTimeMillis()-st);

        s.addOne("关系");

        System.out.println(s.judge("",content));
    }

}
