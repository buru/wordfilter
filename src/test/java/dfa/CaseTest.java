package dfa;

/**
 * @auth bruce-sha
 * @date 2015/6/24
 */
public class CaseTest {

    public static void main(String[] args) {
        String s = "1xDfHwReZ,/";

        s.chars().mapToObj(e -> (char) e).forEach(e -> {
            System.out.println(is(e));
        });

        char c='c';
        System.out.println((int)'c');
        System.out.println((int)'C');

        System.out.println(   Character.toUpperCase('1'));
    }

    private static boolean is(char c) {
        return 'a' <= c && c <= 'z';
    }
}
