package wordfilter.app;

/**
 * 颁布的合法 APP ID
 *
 * @auth bruce-sha
 * @date 2015/6/24
 */
public enum App {
    SIMPLE, SMS, SARDINE, BULUO;
}
