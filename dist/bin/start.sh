#!/bin/sh

PATH=$PATH:$HOME/bin:/sbin:/usr/bin:/usr/sbin:/xxx/jdk/bin
JAVA_HOME=`which java`
#JAVA_OPTS="-Xms1024M -Xmx1024M -Xss1M -XX:MaxPermSize=256M -XX:+UseParallelGC"

APP_HOME=`cd $(dirname $0)/..;pwd`
APP_MAIN="wordfilter.Main"
APP_CLASSPATH="$APP_HOME/jar/*:$APP_HOME/config/*"

kill -9 $(ps -ef|grep $APP_MAIN |gawk '$0 !~/grep/ {print $2}' |tr -s '\n' ' ')

today=`date "+%Y%m%d"`
LOG_FILE=$APP_HOME"/logs/"$today".log"

nohup $JAVA_HOME -Dapp.home=$APP_HOME -cp $APP_CLASSPATH $APP_MAIN >> $LOG_FILE 2>&1 &
echo $!>$APP_HOME"/logs/"app.pid