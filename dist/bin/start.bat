@echo off

set APP_HOME=%~dp0..
set JAVA_OPTS=-Xmx768M -Xms768M -Xss1M -XX:MaxPermSize=128M -XX:+UseParallelGC
set APP_CLASSPATH=%APP_HOME%/jar/*;%APP_HOME%/config
set APP_MAIN="wordfilter.Main"

java %JAVA_OPTS% -cp "%APP_CLASSPATH%" -Dakka.home="%APP_HOME%" akka.kernel.Main %*

java -Dapp.home=$APP_HOME -cp $APP_CLASSPATH $APP_MAIN >> $LOG_FILE 2>&1 &
echo $!>$APP_HOME"/logs/"app.pid